#!/bin/bash
set -o errexit

if [[ ! $1 ]] || [[ ! $2 ]]
  then
    echo "Usage: $0 [ m3u8 URL ] [ result file ]"
    exit 1
fi

if [[ $(uname -s) != 'Linux' ]] && [[ $(uname -s) != 'Darwin' ]]; then
  echo "Unsupported kernel: $(uname -s)"
  exit 1
fi

if [[ ! -r /usr/bin/xxd ]]; then
  echo "This script requires xxd installed"
  exit 1
fi

DATE=$(date +%Y-%m-%d_%H-%M-%S)
TMPDIR=~/tmp/.m3u8-downloader_$DATE
M3U8URL=$1
OUTPUT_FILENAME=$2
PLAYLIST=$TMPDIR/playlist.m3u8

if [[ ! -d "$TMPDIR" ]]
  then
    mkdir -v "$TMPDIR"
fi

# save m3u8 locally
wget "$M3U8URL" -O "$PLAYLIST"

# check m3u8 source, vk and sc require different approach
if grep -m 1 -E 'http|https' "$PLAYLIST" | sed 's/^.*http:\/\///;s/^.*https:\/\///;s/\/.*$//' | grep -q sndcdn
  then
    SOURCE=soundcloud
fi

if grep -m 1 -E 'http|https' "$PLAYLIST" | sed 's/^.*http:\/\///;s/^.*https:\/\///;s/\/.*$//' | grep -q vk
  then
    SOURCE=vk
fi

case "$SOURCE" in
soundcloud)
  for file in $(cat "$PLAYLIST" | sed '/^#/d')
    do
      counter=$((counter+01))
      fails=0
      until [ "$fails" -ge 5 ]
        do
          wget -nv -t 1 -T 4 "$file" -O "$TMPDIR/AUD_$(printf %04d $counter).mp3" && break
          fails=$[fails+1]
          echo "Failed to download $file after $fails attempt(-s), retrying..."
          if [ $fails -ge 5 ]
            then
              echo "Maximum download attempts reached (5), exiting..."
              exit 1
          fi
          sleep 2s
    done
  done
  echo "downloaded $counter items"
  ;;
vk)
  VK_KEY_URL=$(grep -m 1 'key.pub' "$PLAYLIST" | sed 's/^.*URI="//;s/"$//')
  VK_SERVER_URL=$(grep -m 1 -E 'http|https' "$PLAYLIST" | sed 's/^.*http:\/\///;s/^.*https:\/\///;s/\/.*$//')
  VK_URL_PATH=$(grep -m 1 -E 'http|https' "$PLAYLIST" | sed 's/^.*http:\/\///;s/^.*https:\/\///;s/key.*$//;s/'"$VK_SERVER_URL"'//')
  echo "Getting key to decrypt ts files..."
  wget "$VK_KEY_URL" -O "$TMPDIR"/key.pub
  for file in $(cat "$PLAYLIST" | sed '/^#/d')
    do
      counter=$((counter+01))
      fails=0
      until [ "$fails" -ge 5 ]
        do
          wget -nv -t 1 -T 4 https://"$VK_SERVER_URL""$VK_URL_PATH""$file" -O "$TMPDIR/AUD_$(printf %04d $counter)" && break
          fails=$[fails+1]
          echo "Failed to download $file after $fails attempt(-s), retrying..."
          if [ $fails -ge 5 ]
            then
              echo "Maximum download attempts reached (5), exiting..."
              exit 1
          fi
          sleep 2s
    done
  done
  echo "downloaded $counter items"
  ;;
*)
  echo "m3u8 source is unknown, currently supported sources are soundcloud and vk"
  echo "Exiting..."
  rm -v "$PLAYLIST"
  rmdir -v "$TMPDIR"
  exit 1
esac

cd "$TMPDIR"

#concat
case "$SOURCE" in
soundcloud)
  #create list
  ls -1 *.mp3 > filelist
  if [[ $(uname -s) == 'Linux' ]]; then
    sed -i 's/^/file \x27/g;s/$/\x27/g' filelist
  fi
  if [[ $(uname -s) == 'Darwin' ]]; then
    sed -e "s/^/file '/g;s/$/'/g" -i '' filelist
  fi
  echo "Merging media parts..."
  ffmpeg -hide_banner -f concat -i filelist -c copy .output.mp3
  echo "Moving merged file to parent dir..."
  mv -v .output.mp3 ../"$OUTPUT_FILENAME"
  rm "$TMPDIR"/filelist "$TMPDIR"/playlist.m3u8
  ;;
vk)
  echo "Decrypting media..."
  for file in $(ls -1 AUD*)
    do
      if file "$file" | grep -q ': data'
        then
          mv "$file" "$file".dat
          openssl aes-128-cbc -d -nosalt -iv 0 -K $(xxd -p key.pub) -in "$file".dat -out "$file".ts
          rm "$file".dat
      fi
      if file "$file" | grep -q 'MPEG'
        then
          mv "$file" "$file".ts
      fi
  done
  echo "Merging media parts..."
  #create list
  ls -1 *.ts > filelist
  if [[ $(uname -s) == 'Linux' ]]; then
    sed -i 's/^/file \x27/g;s/$/\x27/g' filelist
  fi
  if [[ $(uname -s) == 'Darwin' ]]; then
    sed -e "s/^/file '/g;s/$/'/g" -i '' filelist
  fi
  ffmpeg -hide_banner -f concat -i filelist -c copy .output.mp3
  echo "Moving merged file to parent dir..."
  mv -v .output.mp3 ../"$OUTPUT_FILENAME"
  rm "$TMPDIR"/key.pub "$TMPDIR"/playlist.m3u8 "$TMPDIR"/filelist "$TMPDIR"/*.ts
  ;;
esac

echo "Cleaning up..."
find "$TMPDIR" -name 'AUD*' -delete
rmdir "$TMPDIR"

# Show downloaded file duration
ffprobe -v error -show_entries format=duration -sexagesimal -of default=noprint_wrappers=1:nokey=1 ../"$OUTPUT_FILENAME"

exit 0
