# m3u8 downloader
Tool to download media from SoundCloud and VK

# Compatibility:

• Darwin

• Linux

# Dependencies:

• ffmpeg

• xxd

• openssl

• sed

• wget

# USAGE:
1. Clone m3u8-downloader.sh to your computer (tested on Mac only as of 29/12/2019)
2. Open developer mode in your favourite browser and start playing media on SoundCloud or VK, filter out files on Network tab to 'm3u8', find m3u8 of media you wish to download and copy it's URL
3. Add execution bit: `chmod +x m3u8-downloader.sh`
4. Run with `./m3u8-downloader.sh "URL" "FILENAME"` (remember to add quotation marks) and hit enter

# TODO:
• Check for dependencies before running

• Check on other platforms

• Switch to GNU sed
